---
layout: page
title: "Projects"
description: "List of my projects"
permalink: "/projects/"
---

# Ijkl (Alpha)

A preprocessor for [i3](https://i3wm.org) config file.

[Source](https://github.com/ankitrgadiya/ijkl/)
[Pypi](https://pypi.org/project/ijkl/)

# Nnpy

A very simple and minimal [Flask](http://flask.pocoo.org/) based Pastebin which
uses [Sqlite](https://www.sqlite.org/) database to store pastes.

[Docs](https://argp.in/docs/nnpy/index.html)
[Source](https://github.com/ankitrgadiya/nnpy/)

# Docker Images

* [Cgit](https://goo.gl/hWQXkh) - Containerize cgit web interface for git
  repositories
* [Cloud9](https://goo.gl/MXHTKH) - Cloud9 IDE in Docker
* [Ikiwiki](https://goo.gl/wMe9Ey) - Ikiwiki with cgi enabled in Docker

[Docs](https://argp.in/docs/docker/)
[Source](https://goo.gl/eFXqHX)

# Arch User Repository Packages

* [ruby-html-proofer](https://goo.gl/NEuGjU)
* [ruby-jekyll-archives](https://goo.gl/bcdwpH)
* [ruby-jekyll-compose](https://goo.gl/eoWURi)
* [ruby-jekyll-feed](https://goo.gl/mvB4yG)
* [ruby-jekyll-last-modified-at](https://goo.gl/tQ4id4)
* [ruby-jekyll-paginate-v2](https://goo.gl/Uodn1t)
* [ruby-jekyll-redirect-from](https://goo.gl/g5f3UA)
* [ruby-jekyll-seo-tag](https://goo.gl/tqLXYT)
* [ruby-jekyll-sitemap](https://goo.gl/KV9ziL)
* [sbm](https://goo.gl/eo6krx)
* [sent](https://goo.gl/LhmLUF)
* [skroll](https://goo.gl/cpzakK)
* [slstatus-git](https://goo.gl/NxK2if)
* [stagit](https://goo.gl/AaJik7)
* [stagit-git](https://goo.gl/Jg1H8H)
* [ttf-megrim](https://goo.gl/Kw2FJd)
* [vim-colorscheme-alduin](https://goo.gl/ejZK8P)
* [vim-colorscheme-sierra](https://goo.gl/rbL8u8)
* [vim-ranger](https://goo.gl/MkgcmF)
* [vim-ranger-git](https://goo.gl/pgpJKc)
* [vim-tcomment](https://goo.gl/aY4bPY)
* [vim-tcomment-git](https://goo.gl/xCjAHH)
* [vim-voom](https://goo.gl/kuTjZ9)
* [wificurse](https://goo.gl/cpyuou)

[Source](https://goo.gl/u26mfw)

# Simple Template

A project for making simple yet fully functional templates for static site
generators.

* [Jekyll](https://st.argp.in/jekyll/) - Available as a
  [gem](https://rubygems.org/gems/jekyll-simple-template/)
* Hugo - Coming Soon...
* [Pandoc](https://st.argp.in/pandoc/)

[Website](https://st.argp.in/)
[Source](https://github.com/simple-template/)

# Jekyll Templates

* [Proxima](https://goo.gl/FMu8wE) - Simple blogging template. (&#x26a0;
  Unmaintained)
* [Deneb](https://goo.gl/Lr2EnX) - A mordern blogging template with lots of
  features such as Syntax highlighting, category support, multiple author
  support. (&#x26a0; Unmaintained)
* [Documentation-Template](https://goo.gl/mvq3C2) - A light-weight
  documentation template with Github Markdown based on Bootstrap. (&#x26a0;
  Unmaintained)

# Fossasia GCI 16 Site

[Fossasia](https://www.fossasia.org/)'s Google Code-in 2016 Project website.

[Source](https://goo.gl/oUXVYu)

# Img\_checker

Ruby gem to check for dimension of images. Build during Google Code-in 2016.

[Gem](https://goo.gl/nt7ohh)
[Source](https://goo.gl/5NsJK3)
